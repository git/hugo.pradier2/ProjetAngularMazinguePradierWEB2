# Projet Angular - Gestion des Recettes (Mazingue & Pradier WEB2)

Ce projet est une application Angular permettant de gérer des recettes de cuisine et d'ingrédients.

## Installation

Pour installer ce projet, suivez les étapes suivantes :

1. Clonez le dépôt sur votre machine locale.
   `git clone https://codefirst.iut.uca.fr/githugopradier2ProjetAngularMazinguePradierWEB2.git`
2. Assurez-vous d'avoir la dernière version de Node JS installée sur votre machine. Si ce n'est pas le cas, vous pouvez la télécharger ici:
   `https://nodejs.org/fr/download/package-manager`

   Ou bien vous pouvez le mettre à jour en utilisant la commande suivante :
   `npm install -g npm@latest`

3. Assurez-vous d'avoir Angular CLI :
   `npm install -g @angular/cli`

   Pour vérifier si Angular CLI est installé, exécutez la commande suivante :
   `ng --version`

4. Ouvrez une fenêtre de terminal et naviguez jusqu'au répertoire du projet.
5. Exécutez la commande `npm install` pour installer les dépendances.
6. Ensuite, exécutez la commande `ng serve` pour démarrer le serveur de développement.
7. Vous pouvez désormais accéder à l'application en ouvrant votre navigateur et en accédant à l'URL suivante : `http://localhost:4200`

## Fonctionnalités de l'application (en plus de celles demandées)

Toutes les fonctionnalités demandées ont été implémentées. En plus de ces fonctionnalités, nous avons ajouté les fonctionnalités suivantes (non exaustif):

### 1. Annonce de Connexion Utilisateur

- **Description :** Affiche un message sur la page d'accueil indiquant si l'utilisateur est connecté.
- **Détails :**
  - Si l'utilisateur est connecté, le message indique que l'on est connecté en vert.
  - Si l'utilisateur est déconnecté, le message indique que l'on est déconnecté en rouge.

### 2. Bouton "Back to List" dans le Détail d'une Recette

- **Description :** Ajout d'un bouton permettant de revenir à la liste des recettes depuis la page de détail d'une recette.
- **Détails :**
  - Le bouton "Back to List" facilite la navigation en permettant à l'utilisateur de revenir rapidement à la liste des recettes.

### 3. Édition et Suppression d'une Recette

- **Description :** Permet aux utilisateurs d'éditer ou de supprimer une recette.
- **Détails :**
  - Les utilisateurs peuvent maintenant éditer les détails d'une recette existante.
  - Les utilisateurs peuvent supprimer une recette de la liste.

## Auteurs

Ce projet a été développé par Matis MAZINGUE et Hugo PRADIER.
