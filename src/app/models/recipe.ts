import { Ingredient } from './ingredient';

export class Recipe {
  public id: number;
  public name: string;
  public description: string;
  public image: string;
  public ingredients: Ingredient[];

  constructor(
    id: number,
    name: string,
    description: string,
    image: string,
    ingredients: Ingredient[]
  ) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.image = image;
    this.ingredients = ingredients;
  }

  public setId(id: number) {
    this.id = id;
  }
}
