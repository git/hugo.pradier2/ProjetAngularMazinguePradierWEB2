export class IngredientRecipe {
  public idIngredient: number;
  public idRecipe: number;
  public quantity: number;

  constructor(idIngredient: number, idRecipe: number, quantity: number) {
    this.idIngredient = idIngredient;
    this.idRecipe = idRecipe;
    this.quantity = quantity;
  }
}
