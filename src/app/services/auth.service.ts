import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly adminKey = 'isAdmin';
  private loggedInSubject: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(this.isLoggedIn());

  get loggedIn$(): Observable<boolean> {
    return this.loggedInSubject.asObservable();
  }

  login() {
    localStorage.setItem(this.adminKey, 'true');
    this.loggedInSubject.next(true);
  }

  logout() {
    localStorage.removeItem(this.adminKey);
    this.loggedInSubject.next(false);
  }

  isLoggedIn(): boolean {
    return localStorage.getItem(this.adminKey) === 'true';
  }
}
