import { Injectable } from '@angular/core';
import { Ingredient } from '../models/ingredient';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class IngredientService {
  private ingredients: Ingredient[] = [
    new Ingredient(1, 'Butter'),
    new Ingredient(2, 'Flour'),
    new Ingredient(3, 'Pepper'),
    new Ingredient(4, 'Salt'),
    new Ingredient(5, 'Grated cheese'),
    new Ingredient(6, 'Abraded nutmeg'),
    new Ingredient(7, 'Thyme'),
    new Ingredient(8, 'Bay leaf'),
    new Ingredient(9, 'Milk'),
    new Ingredient(10, 'Red wine'),
    new Ingredient(11, 'Olive oil'),
    new Ingredient(12, 'Tomato purée'),
    new Ingredient(13, 'Yellow onion'),
    new Ingredient(14, 'Carrot'),
    new Ingredient(15, 'Lasagna'),
    new Ingredient(16, 'Ground beef'),
    new Ingredient(17, 'Garlic'),
    new Ingredient(18, 'Celery'),
    new Ingredient(19, 'Water'),
    new Ingredient(20, 'Basilic'),
    new Ingredient(21, 'Parmesan'),
  ];

  getIngredients(): Ingredient[] {
    return this.ingredients;
  }

  getIngredient(id: number): Ingredient {
    return this.ingredients.find(
      (ingredient) => ingredient.id === id
    ) as Ingredient;
  }

  addIngredient(name: string): void {
    const newId = Math.max(...this.ingredients.map(ingredient => ingredient.id)) + 1;
    const newIngredient = new Ingredient(newId, name);
    this.ingredients.push(newIngredient);
  }
}

// version api
// @Injectable({
//   providedIn: 'root',
// })
// export class IngredientService {
//   private apiUrl = 'https://664ba07f35bbda10987d9f99.mockapi.io/api/ingrédients';

//   constructor(private http: HttpClient) {}

//   getIngredients(): Observable<Ingredient[]> {
//     return this.http.get<Ingredient[]>(this.apiUrl);
//   }

//   getIngredient(id: number): Observable<Ingredient> {
//     return this.http.get<Ingredient>(`${this.apiUrl}/${id}`);
//   }

//   addIngredient(ingredient: Ingredient): Observable<Ingredient> {
//     return this.http.post<Ingredient>(this.apiUrl, ingredient);
//   }

//   updateIngredient(id: number, ingredient: Ingredient): Observable<Ingredient> {
//     return this.http.put<Ingredient>(`${this.apiUrl}/${id}`, ingredient);
//   }

//   deleteIngredient(id: number): Observable<void> {
//     return this.http.delete<void>(`${this.apiUrl}/${id}`);
//   }
// }
