import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

interface MenuItem {
  label: string;
  link: string;
}

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  private menuItems: MenuItem[] = [
    { label: 'Home', link: '/home' },
    { label: 'List recipes', link: '/list' },
    { label: 'Add new recipe', link: '/add' },
    { label: 'List ingredients', link: '/ingredients' },
  ];

  constructor(private authService: AuthService) {}

  getMenuItems(): Observable<MenuItem[]> {
    return new Observable((observer) => {
      this.authService.loggedIn$.subscribe((isLoggedIn) => {
        const authItems = isLoggedIn
          ? [{ label: 'Logout', link: '/logout' }]
          : [{ label: 'Login', link: '/login' }];

        observer.next([...this.menuItems, ...authItems]);
      });
    });
  }
}
