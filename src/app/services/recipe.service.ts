import { Injectable } from '@angular/core';
import { Recipe } from '../models/recipe';
import { IngredientRecipe } from '../models/ingredient-recipe';

@Injectable({
  providedIn: 'root',
})
export class RecipeService {
  private localStorageKey = 'recipes';
  private ingredientRecipeKey = 'ingredientRecipes';
  private currentOrdersKey = 'currentOrders';

  getRecipes(): Recipe[] {
    const recipesJson = localStorage.getItem(this.localStorageKey);
    return recipesJson ? JSON.parse(recipesJson) : [];
  }

  addRecipe(recipe: Recipe): void {
    const recipes = this.getRecipes();
    recipes.push(recipe);
    localStorage.setItem(this.localStorageKey, JSON.stringify(recipes));
  }

  deleteRecipe(recipeId: number): void {
    let recipes = this.getRecipes();
    const deletedRecipe = recipes.find((recipe) => recipe.id === recipeId);

    if (deletedRecipe) {
      recipes = recipes.filter((recipe) => recipe.id !== recipeId);
      localStorage.setItem(this.localStorageKey, JSON.stringify(recipes));

      let ingredientRecipes = this.getIngredientRecipes(recipeId);
      ingredientRecipes = ingredientRecipes.filter(
        (ir) => ir.idRecipe !== recipeId
      );
      localStorage.setItem(
        this.ingredientRecipeKey,
        JSON.stringify(ingredientRecipes)
      );

      let currentOrders = this.getCurrentOrders();
      currentOrders = currentOrders.filter((order) => order.id !== recipeId);
      localStorage.setItem(
        this.currentOrdersKey,
        JSON.stringify(currentOrders)
      );
    }
  }

  getIngredientRecipes(recipeId: number): IngredientRecipe[] {
    const ingredientRecipes: IngredientRecipe[] = JSON.parse(
      localStorage.getItem(this.ingredientRecipeKey) || '[]'
    );
    return ingredientRecipes.filter((ir) => ir.idRecipe === recipeId);
  }

  addIngredientRecipe(ingredientRecipe: IngredientRecipe): void {
    const ingredientRecipes: IngredientRecipe[] = JSON.parse(
      localStorage.getItem(this.ingredientRecipeKey) || '[]'
    );
    ingredientRecipes.push(ingredientRecipe);
    localStorage.setItem(
      this.ingredientRecipeKey,
      JSON.stringify(ingredientRecipes)
    );
  }

  updateIngredientRecipe(updatedIngredientRecipe: IngredientRecipe): void {
    let ingredientRecipes: IngredientRecipe[] = JSON.parse(
      localStorage.getItem(this.ingredientRecipeKey) || '[]'
    );

    const index = ingredientRecipes.findIndex(
      (ir) =>
        ir.idIngredient === updatedIngredientRecipe.idIngredient &&
        ir.idRecipe === updatedIngredientRecipe.idRecipe
    );

    if (index !== -1) {
      ingredientRecipes[index] = updatedIngredientRecipe;
    } else {
      ingredientRecipes.push(updatedIngredientRecipe);
    }

    localStorage.setItem(
      this.ingredientRecipeKey,
      JSON.stringify(ingredientRecipes)
    );
  }

  getIngredientQuantity(
    ingredientId: number,
    ingredientRecipes: IngredientRecipe[]
  ): number {
    const ingredientRecipe = ingredientRecipes.find(
      (ir) => ir.idIngredient === ingredientId
    );
    return ingredientRecipe ? ingredientRecipe.quantity : 0;
  }

  orderRecipe(recipe: Recipe): void {
    let currentOrders: Recipe[] = JSON.parse(
      localStorage.getItem(this.currentOrdersKey) || '[]'
    );
    currentOrders.push(recipe);
    localStorage.setItem(this.currentOrdersKey, JSON.stringify(currentOrders));
  }

  getCurrentOrders(): Recipe[] {
    const currentOrdersJson = localStorage.getItem(this.currentOrdersKey);
    return currentOrdersJson ? JSON.parse(currentOrdersJson) : [];
  }

  updateRecipe(recipe: Recipe): void {
    let recipes = this.getRecipes();
    const index = recipes.findIndex((r) => r.id === recipe.id);
    if (index !== -1) {
      recipes[index] = recipe;
      localStorage.setItem(this.localStorageKey, JSON.stringify(recipes));
    }
  }

  getRecipeById(recipeId: number): Recipe | undefined {
    const recipes = this.getRecipes();
    return recipes.find((recipe) => recipe.id === recipeId);
  }
}
