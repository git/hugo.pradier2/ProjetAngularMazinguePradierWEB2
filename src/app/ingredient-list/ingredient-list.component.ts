import { Component, OnInit, ViewChild } from '@angular/core';
import { Ingredient } from '../models/ingredient';
import { IngredientService } from '../services/ingredient.service';
import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-ingredient-list',
  standalone: true,
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.scss'],
  imports: [CommonModule, MatTableModule, MatPaginatorModule, HttpClientModule],
})
export class IngredientListComponent implements OnInit {
  ingredients: Ingredient[] = [];
  dataSource: MatTableDataSource<Ingredient> =
    new MatTableDataSource<Ingredient>();
  displayedColumns: string[] = ['id', 'name'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(
    private ingredientService: IngredientService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.ingredients = this.ingredientService.getIngredients();
    this.dataSource.data = this.ingredients;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  // version api

  // ngOnInit(): void {
  //   this.loadIngredients();
  // }

  // loadIngredients(): void {
  //   this.ingredientService.getIngredients().subscribe((ingredients) => {
  //     this.ingredients = ingredients;
  //     this.dataSource.data = this.ingredients;
  //   });
  // }

  onEditIngredient(ingredient: Ingredient): void {
    this.router.navigate(['/ingredient', ingredient.id, 'edit']);
  }

  navToAddIngredient(): void {
    this.router.navigate(['/ingredient/add']);
  }
}
