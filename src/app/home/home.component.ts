import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Recipe } from '../models/recipe';
import { RecipeService } from '../services/recipe.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent implements OnInit {
  currentOrders: Recipe[] = [];
  isLoggedIn: boolean = false;

  constructor(
    private recipeService: RecipeService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loadCurrentOrders();
  }

  loadCurrentOrders(): void {
    this.currentOrders = this.recipeService.getCurrentOrders();
    this.isLoggedIn = this.authService.isLoggedIn();
  }

  onSelectRecipe(recipe: Recipe): void {
    this.router.navigate(['/recipe', recipe.id]);
  }
}
