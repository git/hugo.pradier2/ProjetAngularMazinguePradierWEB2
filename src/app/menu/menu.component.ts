import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MenuService } from '../services/menu.service';
import { CommonModule } from '@angular/common';
import { RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [MatButtonModule, MatMenuModule, CommonModule, RouterLink],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.scss',
})
export class MenuComponent implements OnInit {
  menuItems: { label: string; link: string }[] | undefined;

  private subscription: Subscription = new Subscription();

  constructor(private menuService: MenuService) {}

  ngOnInit(): void {
    this.subscription = this.menuService.getMenuItems().subscribe((items) => {
      this.menuItems = items;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
