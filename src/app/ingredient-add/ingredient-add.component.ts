import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IngredientService } from '../services/ingredient.service';
import { Ingredient } from '../models/ingredient';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-ingredient-add',
  standalone: true,
  imports: [
    HttpClientModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
  ],
  templateUrl: './ingredient-add.component.html',
  styleUrl: './ingredient-add.component.scss',
})
export class IngredientAddComponent {
  // version api
  // ingredient: Ingredient = new Ingredient(0, '');
  name: string = '';

  constructor(
    private ingredientService: IngredientService,
    private router: Router
  ) {}

  addIngredient(): void {
    if (this.name.trim()) {
      this.ingredientService.addIngredient(this.name.trim());
      this.router.navigate(['/ingredients']);
    }
  }

  // version api
  // addIngredient(): void {
  //   this.ingredientService.addIngredient(this.ingredient).subscribe(() => {
  //     this.router.navigate(['/ingredients']);
  //   });
  // }
}
