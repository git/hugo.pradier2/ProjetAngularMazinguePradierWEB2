import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ingredient } from '../models/ingredient';
import { IngredientService } from '../services/ingredient.service';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';

@Component({
  selector: 'app-ingredient-edit',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    MatInputModule,
  ],
  templateUrl: './ingredient-edit.component.html',
  styleUrls: ['./ingredient-edit.component.scss'],
})
export class IngredientEditComponent implements OnInit {
  ingredient!: Ingredient;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private ingredientService: IngredientService
  ) {}

  ngOnInit(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.ingredient = this.ingredientService.getIngredient(id);
  }

  save(): void {
    this.router.navigate(['/ingredients']);
  }

  // version api
  // ngOnInit(): void {
  //   const id = Number(this.route.snapshot.paramMap.get('id'));
  //   this.ingredientService.getIngredient(id).subscribe((ingredient) => {
  //     this.ingredient = ingredient;
  //   });
  // }

  // save(): void {
  //   this.ingredientService.updateIngredient(this.ingredient.id, this.ingredient).subscribe(() => {
  //     this.router.navigate(['/ingredients']);
  //   });
  // }
}
