import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Recipe } from '../models/recipe';
import { IngredientRecipe } from '../models/ingredient-recipe';
import { RecipeService } from '../services/recipe.service';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-recipe',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatCardModule],
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss'],
})
export class RecipeComponent implements OnInit {
  recipe: Recipe | undefined;
  ingredientRecipes: IngredientRecipe[] = [];
  formattedDescription: string = '';

  constructor(
    private route: ActivatedRoute,
    private recipeService: RecipeService
  ) {}

  ngOnInit(): void {
    const recipeId = this.route.snapshot.paramMap.get('id');
    this.recipe = this.recipeService.getRecipes().find(
      (r) => r.id.toString() === recipeId
    );

    if (this.recipe) {
      this.ingredientRecipes = this.recipeService.getIngredientRecipes(this.recipe.id);
      this.formattedDescription = this.formatDescription(
        this.recipe.description
      );
    }
  }

  getIngredientQuantity(ingredientId: number): number {
    return this.recipeService.getIngredientQuantity(ingredientId, this.ingredientRecipes);
  }

  navBack(): void {
    window.history.back();
  }

  formatDescription(description: string): string {
    return description.replace(/(.{50})/g, '$1<br/>');
  }
}
