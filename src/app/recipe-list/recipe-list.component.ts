import { Component, OnInit, ViewChild } from '@angular/core';
import { Recipe } from '../models/recipe';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { TruncatePipe } from '../pipes/truncate.pipe';
import { RecipeService } from '../services/recipe.service';

@Component({
  selector: 'app-recipe-list',
  standalone: true,
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss'],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatButtonModule,
    TruncatePipe,
  ],
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [];
  dataSource: MatTableDataSource<Recipe> = new MatTableDataSource<Recipe>();
  displayedColumns: string[] = ['name', 'description', 'image', 'actions'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private router: Router, private recipeService: RecipeService) {}

  ngOnInit(): void {
    this.recipes = this.recipeService.getRecipes();
    this.dataSource.data = this.recipes;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  navToAddRecipe(): void {
    this.router.navigate(['/add']);
  }

  onSelectRecipe(recipe: Recipe): void {
    this.router.navigate(['/recipe', recipe.id]);
  }

  onDeleteRecipe(recipe: Recipe): void {
    this.recipeService.deleteRecipe(recipe.id);
    this.recipes = this.recipeService.getRecipes();
    this.dataSource.data = this.recipes;
  }

  onOrderRecipe(recipe: Recipe): void {
    this.recipeService.orderRecipe(recipe);
    this.router.navigate(['/home']);
  }

  onEditRecipe(recipe: Recipe): void {
    this.router.navigate(['/recipe', recipe.id, 'edit']);
  }
}
