import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, NgForm } from '@angular/forms';
import { RecipeService } from '../services/recipe.service';
import { IngredientService } from '../services/ingredient.service';
import { Recipe } from '../models/recipe';
import { IngredientRecipe } from '../models/ingredient-recipe';
import { Ingredient } from '../models/ingredient';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-recipe-edit',
  standalone: true,
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss'],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
})
export class RecipeEditComponent implements OnInit {
  recipe: Recipe = new Recipe(0, '', '', '', []);
  ingredients: Ingredient[] = [];
  ingredientQuantities: { [key: number]: number } = {};
  selectedIngredient: number | null = null;
  imageError: string | null = null;
  maxDescriptionLength: number = 200;
  isSubmitting: boolean = false;
  @ViewChild('recipeForm', { static: true }) recipeForm!: NgForm;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private recipeService: RecipeService,
    private ingredientService: IngredientService
  ) {}

  ngOnInit(): void {
    this.ingredients = this.ingredientService.getIngredients();
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      const recipeId = parseInt(id, 10);
      this.recipe =
        this.recipeService.getRecipeById(recipeId) ??
        new Recipe(0, '', '', '', []);
      this.loadIngredientQuantities(recipeId);
    }
  }

  onFileChange(event: any): void {
    const file = event.target.files[0];
    if (file) {
      const validImageTypes = [
        'image/jpeg',
        'image/png',
        'image/gif',
        'image/jpg',
      ];
      if (!validImageTypes.includes(file.type)) {
        this.imageError = 'Invalid file type. Please select an image file.';
        return;
      }

      this.imageError = null;

      const reader = new FileReader();
      reader.onload = () => {
        this.recipe.image = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  loadIngredientQuantities(recipeId: number): void {
    const ingredientRecipes = this.recipeService.getIngredientRecipes(recipeId);
    ingredientRecipes.forEach((ingredientRecipe) => {
      this.ingredientQuantities[ingredientRecipe.idIngredient] =
        ingredientRecipe.quantity;
    });
  }

  addIngredient(): void {
    if (
      this.selectedIngredient !== null &&
      !this.ingredientQuantities[this.selectedIngredient]
    ) {
      this.ingredientQuantities[this.selectedIngredient] = 1;
      this.selectedIngredient = null;
    }
  }

  removeIngredient(id: number): void {
    delete this.ingredientQuantities[id];
  }

  getIngredientKeys(): number[] {
    return Object.keys(this.ingredientQuantities).map((key) =>
      parseInt(key, 10)
    );
  }

  findIngredientName(id: number): string {
    const ingredient = this.ingredients.find(
      (ingredient) => ingredient.id === id
    );
    return ingredient ? ingredient.name : '';
  }

  onSubmit(): void {
    if (this.isSubmitting) return;

    this.isSubmitting = true;

    this.recipe.ingredients = this.getIngredientKeys().map((id) => {
      const ingredient = this.ingredientService.getIngredient(id);
      return new Ingredient(id, ingredient.name);
    });

    this.recipeService.updateRecipe(this.recipe);

    this.getIngredientKeys().forEach((id) => {
      const quantity = this.ingredientQuantities[id];
      const ingredientRecipe = new IngredientRecipe(
        id,
        this.recipe.id,
        quantity
      );
      this.recipeService.updateIngredientRecipe(ingredientRecipe);
    });

    this.router.navigate(['/list']).then(() => {
      this.isSubmitting = false;
    });
  }
}
