import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  template: ``,
})
export class LoginComponent {
  constructor(private authService: AuthService, private router: Router) {
    this.authService.login();
    this.router.navigate(['/ingredients']);
  }
}
