import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { IngredientService } from '../services/ingredient.service';
import { Ingredient } from '../models/ingredient';
import { Recipe } from '../models/recipe';
import { Router } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { RecipeService } from '../services/recipe.service';
import { IngredientRecipe } from '../models/ingredient-recipe';

@Component({
  selector: 'app-recipe-add',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  templateUrl: './recipe-add.component.html',
  styleUrls: ['./recipe-add.component.scss'],
})
export class RecipeAddComponent implements OnInit {
  recipe: Recipe = new Recipe(0, '', '', '', []);
  ingredients: Ingredient[] = [];
  ingredientQuantities: { [key: number]: number } = {};
  selectedIngredient: number | null = null;
  imageError: string | null = null;
  maxDescriptionLength: number = 200;
  isSubmitting: boolean = false;

  constructor(
    private ingredientService: IngredientService,
    private recipeService: RecipeService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.ingredients = this.ingredientService.getIngredients();
  }

  onFileChange(event: any): void {
    const file = event.target.files[0];
    if (file) {
      const validImageTypes = [
        'image/jpeg',
        'image/png',
        'image/gif',
        'image/jpg',
      ];
      if (!validImageTypes.includes(file.type)) {
        this.imageError = 'Invalid file type. Please select an image file.';
        return;
      }

      this.imageError = null;

      const reader = new FileReader();
      reader.onload = () => {
        this.recipe.image = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  addIngredient(): void {
    if (
      this.selectedIngredient !== null &&
      !this.ingredientQuantities[this.selectedIngredient]
    ) {
      this.ingredientQuantities[this.selectedIngredient] = 1;
      this.selectedIngredient = null;
    }
  }

  removeIngredient(id: number): void {
    delete this.ingredientQuantities[id];
  }

  getIngredientKeys(): number[] {
    return Object.keys(this.ingredientQuantities).map((key) =>
      parseInt(key, 10)
    );
  }

  findIngredientName(id: number): string {
    const ingredient = this.ingredients.find(
      (ingredient) => ingredient.id === id
    );
    return ingredient ? ingredient.name : '';
  }

  onSubmit(): void {
    if (this.isSubmitting) return;

    this.isSubmitting = true;

    const recipes = this.recipeService.getRecipes();

    this.recipe.id = recipes.length ? recipes[recipes.length - 1].id + 1 : 1;

    this.recipe.ingredients = this.getIngredientKeys().map((id) => {
      const ingredient = this.ingredientService.getIngredient(id);
      return new Ingredient(id, ingredient.name);
    });

    this.recipeService.addRecipe(this.recipe);

    this.getIngredientKeys().forEach((id) => {
      const quantity = this.ingredientQuantities[id];
      const ingredientRecipe = new IngredientRecipe(
        id,
        this.recipe.id,
        quantity
      );
      this.recipeService.addIngredientRecipe(ingredientRecipe);
    });

    this.router.navigate(['/list']).then(() => {
      this.isSubmitting = false;
    });
  }
}
