import { Routes } from '@angular/router';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { HomeComponent } from './home/home.component';
import { RecipeAddComponent } from './recipe-add/recipe-add.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './guards/auth.guard';
import { IngredientAddComponent } from './ingredient-add/ingredient-add.component';
import { IngredientEditComponent } from './ingredient-edit/ingredient-edit.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';

export const routes: Routes = [
  { path: 'add', component: RecipeAddComponent },
  { path: 'recipe/:id', component: RecipeComponent },
  { path: 'recipe/:id/edit', component: RecipeEditComponent },
  { path: 'list', component: RecipeListComponent },
  {
    path: 'ingredients',
    component: IngredientListComponent,
    canActivate: [authGuard],
  },
  { path: 'ingredient/add', component: IngredientAddComponent },
  { path: 'ingredient/:id/edit', component: IngredientEditComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: '', component: HomeComponent },
  { path: '**', redirectTo: '' },
];
export class AppRoutingModule {}
